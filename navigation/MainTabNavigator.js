import React from 'react';
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import HomeScreen from "../screens/HomeScreen";
import SettingsScreen from "../screens/SettingsScreen";
import { createAppContainer } from "react-navigation";
import Icon from 'react-native-vector-icons/Ionicons';
import { createStackNavigator } from 'react-navigation-stack';
import InfosArticlesScreen from '../screens/InfosArticlesScreen';


const infosNiewsNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            title: 'ACCUEIL'
        }
    },
    InfosArticle: {
        screen: InfosArticlesScreen,
        navigationOptions: {
            title: 'ARTICLE'
        }
    }
},
    {
        initialRouteName: 'Home'
    }
);

const tabNavigator = createMaterialBottomTabNavigator({
    Home: {
        screen: infosNiewsNavigator,
        navigationOptions: {
            tabBarLabel: 'Accueil',
            tabBarIcon: ({ tintColor }) => (
                <Icon color={tintColor} size={25} name={'ios-home'} />
            ),
            barStyle: { backgroundColor: '#6a89cc' }
        }
    },
    Settings: {
        screen: SettingsScreen,
        navigationOptions: {
            tabBarLabel: 'Paramètres',
            tabBarIcon: ({ tintColor }) => (
                <Icon color={tintColor} size={25} name={'ios-settings'} />
            ),
            barStyle: { backgroundColor: '#82ccdd' }
        }
    }
},
    {
        initialRouteName: 'Home'
    }
);

export default createAppContainer(tabNavigator, infosNiewsNavigator);
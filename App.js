import React from 'react';
import { StyleSheet, Text, View, StatusBar, SafeAreaView, Platform } from 'react-native';
import MainTabNavigator from './navigation/MainTabNavigator';

class App extends React.Component {

  render() {
    return (
      <>
        <SafeAreaView style={styles.safeAreaView}>
        {/* <StatusBar hidden={true} /> */}
          <MainTabNavigator/>
        </SafeAreaView>

      </>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  safeAreaView: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 25 : 0,
  }
});

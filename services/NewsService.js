import axios from 'axios';

//POWERERD BY NEWS API
const key = '00fb49d3b4b244298596f821996a83b4';
const url = `https://newsapi.org/v2/top-headlines?country=fr&apiKey=${key}`;

export const categoryNames = {
    "BUSIN":"business",
    "DIVER":"entertainment",
    "SANT":"health",
    "SCNC":"science",
    "SPRT":"sports",
    "TECH":"technology",
}

class NewsService {

    getNewsByCategory(category = Categories.BUSIN) {
        return axios.get(`${url}&category=${category}`);
    }
}

export default NewsService;
import React, { Component } from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import { CheckBox } from 'react-native-elements'
import { screenHeight } from '../styles/globalstyles';



export default class SettingsScreen extends Component {

    state = {
        Categories: {
            busin: false,
            diver: false,
            sant: false,
            scnc: false,
            sprt: false,
            tech: false,
        }
    };

    save = () => {
        //console.log(this.state.cityName);
        AsyncStorage.getItem('CATEGORIES').then(data => {
            let tab = [];
            if (data != null) {
                tab = JSON.parse(data);
            }
            tab.push(this.state.cityName);
            AsyncStorage.setItem('CATEGORIES', JSON.stringify(tab)).then(() => {
            });
        });

    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: screenHeight * 0.1 }}>Catégories:</Text>
                </View>
                <View style={{ flex: 6, flexDirection: 'column', justifyContent: 'center' }}>
                    <CheckBox
                        title='Économie'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.busin}
                        onPress={() => this.setState({ busin: !this.state.busin })}
                    />
                    <CheckBox
                        title='Divertissement'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.diver}
                        onPress={() => this.setState({ diver: !this.state.diver })}
                    />
                    <CheckBox
                        title='Santé'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.sant}
                        onPress={() => this.setState({ sant: !this.state.sant })}
                    />
                    <CheckBox
                        title='Science'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.scnc}
                        onPress={() => this.setState({ scnc: !this.state.scnc })}
                    />
                    <CheckBox
                        title='Sport'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.sprt}
                        onPress={() => this.setState({ sprt: !this.state.sprt })}
                    />
                    <CheckBox
                        title='Technologie'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.tech}
                        onPress={() => this.setState({ tech: !this.state.tech })}
                    />
                </View>
            </View>
        )
    }
}

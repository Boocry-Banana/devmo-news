import React, { Component } from 'react';
import { TextInput, Button, View, AsyncStorage, Text, Dimensions, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

class InfosArticlesScreen extends Component {

    static navigationOptions = (e) => {
        return {
            title: 'Infos Article'
        }
    }

    render() {

        const screenWidth = Math.round(Dimensions.get('window').width);
        const screenHeight = Math.round(Dimensions.get('window').height);

        return (
            <ScrollView style={{flex: 1, flexDirection: 'column'}}>
                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                    <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center',alignItems: 'center', height: screenHeight*0.1}}>
                        <Text style={{fontSize: screenHeight*0.02, justifyContent: 'center',alignItems: 'center'}}>{this.props.navigation.state.params.title}</Text>
                    </View>
                    <View style={{flex: 2}}>
                        <Image
                            style={{ width: screenWidth * 0.95, height: screenHeight * 0.1 }}
                            resizedMode='cover'
                            source={{ uri: this.props.navigation.state.params.image }}
                        />
                    </View>
                    <View style={{flex: 3}}>
                        <Text>{this.props.navigation.state.params.content}</Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text>{this.props.navigation.state.params.author}</Text>
                        <Text>{this.props.navigation.state.params.date.split('T')[0]}</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }

}

export default InfosArticlesScreen;
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements'
import NewsService, { categoryNames } from '../services/NewsService';
import Loading from '../components/Loading';


export default class HomeScreen extends Component {


    state = { data: null };

    serv = new NewsService();
    screenWidth = Math.round(Dimensions.get('window').width);
    screenHeight = Math.round(Dimensions.get('window').height);

    componentDidMount() {
        this.serv.getNewsByCategory(categoryNames.TECH).then(resp => {
            this.setState({ data: resp.data });
            console.log(this.state.data)
        });
    }

    ReturnFlatList = () => {
        return <FlatList
            data={this.state.data.articles}
            keyExtractor={(item) => item.url}
            renderItem={({ item }) =>
                <Card style={{ elevation: 10 }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('InfosArticle', { title: item.title, image: item.urlToImage, date: item.publishedAt, author: item.author, content: item.content }) }}>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            {item.urlToImage ? (
                                <Image
                                    style={{ width: this.screenWidth * 0.85, height: this.screenHeight * 0.1 }}
                                    resizedMode='cover'
                                    source={{ uri: item.urlToImage }}
                                />
                            ) : (
                                    <Image
                                        style={{ width: this.screenWidth * 0.80, height: this.screenHeight * 0.1 }}
                                        resizedMode='cover'
                                        source={require('../assets/news.jpg')}
                                    />
                                )}
                            <Text>{item.title}</Text>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text>{item.publishedAt.split('T')[0]}</Text>
                                {item.author ? (
                                    <Text>{item.author}</Text>
                                ) : (
                                        <Text>Auteur inconnu</Text>
                                    )}
                            </View>
                        </View>
                    </TouchableOpacity>
                </Card>}
        />;
    };

    render() {

        const screenWidth = Math.round(Dimensions.get('window').width);
        const screenHeight = Math.round(Dimensions.get('window').height);

        return (

            <View style={styles.container}>
                {this.state.data ? (

                    <>
                        <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'blue', alignItems: 'center', justifyContent: 'center' }}>
                            <ScrollView style={styles.scrollView, { backgroundColor: '#60a3bc' }}>
                                {this.ReturnFlatList()}
                            </ScrollView>
                        </View>
                    </>
                ) : (
                        <Loading displayColor="orange">
                        </Loading>
                    )
                }
            </View>
        );
    }

}


const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column'
    },
    card: {
        flex: 1,
        backgroundColor: 'yellow',
        alignItems: 'center'
    },
    scrollView: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    }
});